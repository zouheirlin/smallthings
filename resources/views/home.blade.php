@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div id="userData" class="animate fadeInDown">
                <div class="text-center">
                    <div class="nameCircle mb-2">
                        {{ $user->first_name[0] }}
                    </div>
                    <h3 class="text-uppercase">welcome {{ $user->first_name}}</h3>
                </div>
                <dl class="row mt-5">
                    <dt class="col-sm-3">Full Name</dt>
                    <dd class="col-sm-9 text-uppercase">{{ $user->first_name }} {{ $user->last_name }}</dd>

                    <dt class="col-sm-3">Mobile</dt>
                    <dd class="col-sm-9">0{{ $user->mobile }}</dd>

                    <dt class="col-sm-3">Username</dt>
                    <dd class="col-sm-9">{{ $user->username }}</dd>

                    <dt class="col-sm-3">Email</dt>
                    <dd class="col-sm-9">{{ $user->email }}</dd>
                </dl>
                <button onclick="editForm()" class="btn btn-primary px-3">Edit</button>
            </div>
            
            
            <form id="editForm" class="animate fadeInDown" action="{{url('/user')}}" method="post" style="display:none;">
                @csrf
                <h3>Edit Form</h3>
                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label font-weight-bold">First Name</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" name="first_name" id="first_name" value="{{ $user->first_name }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="last_name" class="col-sm-2 col-form-label font-weight-bold">Last Name</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" name="last_name" id="last_name" value="{{ $user->last_name }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="mobile" class="col-sm-2 col-form-label font-weight-bold">Mobile</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" name="mobile" id="mobile" value="{{ $user->mobile }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="username" class="col-sm-2 col-form-label font-weight-bold">Username</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" name="username" id="username" value="{{ $user->username }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label font-weight-bold">Email</label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control" name="email" id="email" value="{{ $user->email }}">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </form>
        </div>
    </div>
</div>
@endsection
