<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class HomeController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        return view('home', ['user' => $user]);
    }

    public function editUser(Request $request){
        $user = User::where('email','=',$request->input('email'))->first();
        if($user){
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->username = $request->input('username');
            $user->mobile = $request->input('mobile');
            $user->save();
            return redirect('/home');
        }
        return redirect('/home');
    }
}
